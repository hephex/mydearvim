
call plug#begin('~/.vim/plugged')

Plug 'w0rp/ale'
Plug 'altercation/vim-colors-solarized'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'rust-lang/rust.vim'
Plug 'hdima/python-syntax'
Plug 'fatih/vim-go'
Plug 'easymotion/vim-easymotion'
Plug 'cespare/vim-toml'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'rhysd/committia.vim'
Plug 'airblade/vim-gitgutter'
Plug 'vim-scripts/dbext.vim'
Plug 'aquach/vim-http-client'
Plug 'jparise/vim-graphql'
" Plug 'ambv/black'
Plug 'prettier/vim-prettier', { 'do': 'yarn install' }
Plug 'tjvr/vim-nearley'
Plug 'sirver/ultisnips'
Plug 'honza/vim-snippets'
Plug 'majutsushi/tagbar'
Plug 'z0mbix/vim-shfmt', { 'for': 'sh' }
Plug 'racer-rust/vim-racer'
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'Valloric/YouCompleteMe', { 'do': './install.py' }

call plug#end()


set encoding=utf-8
set termencoding=utf-8
set fileencodings=ucs-bom,utf-8,latin1

set background=dark

" maximum number of colors that can be displayed by the host terminal
set t_Co=256
set term=screen-256color

" colorful files
syntax enable

" show line number
set number

" show length limit
set colorcolumn=80

set hidden
set history=500

filetype indent on
set nowrap
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set autoindent
set signcolumn=yes
set backspace=2

" use buffer + for system clipboard
set clipboard=unnamedplus

" highlight found words
set hlsearch

" highlight matching parenthesis
set showmatch

" tabs
nnoremap th  :tabfirst<CR>
nnoremap tj  :tabnext<CR>
nnoremap tk  :tabprev<CR>
nnoremap tl  :tablast<CR>
nnoremap tt  :tabedit<Space>
nnoremap tn  :tabnext<Space>
nnoremap tm  :tabm<Space>
nnoremap td  :tabclose<CR>

""" Command-T """
" set wildignore+=*.log,*.sql,*.cache
let g:CommandTFileScanner='find'

""" NerdCommenter """
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1

" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }

" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

""" NerdTree """
map <C-n> :NERDTreeToggle<CR>

""" Airline """
set laststatus=2

""" Solarized Scheme """
let g:solarized_termcolors=256
" let g:solarized_visibility = "high"
" let g:solarized_contrast = "high"
colorscheme solarized

""" Python Syntax """
let python_highlight_all = 1

""" show all whitespaces """
set list
if has("patch-7.4.710")
  set listchars=tab:>-,trail:~,extends:>,precedes:<,space:.
else
  set listchars=tab:>-,trail:~,extends:>,precedes:<
endif
hi SpecialKey ctermfg=236
hi SpecialKey ctermbg=234

""" vim-rust
let g:rustfmt_autosave = 1
let g:LanguageClient_serverCommands = { 'rust': ['rustup', 'run', 'nightly', 'rls'] }

""" vim-go
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_fields = 1
let g:go_highlight_types = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1

" Disable opening browser after posting your snippet to play.golang.org
let g:go_play_open_browser = 0

""" indentation settings
autocmd Filetype python setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4
autocmd Filetype javascript setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd Filetype yaml setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2

""" fzf

" --column: Show column number
" --line-number: Show line number
" --no-heading: Do not show file headings in results
" --fixed-strings: Search term as a literal string
" --ignore-case: Case insensitive search
" --no-ignore: Do not respect .gitignore, etc...
" --hidden: Search hidden files and folders
" --follow: Follow symlinks
" --glob: Additional conditions for search (in this case ignore everything
"         in the .git/ folder)
" --color: Search color options

command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>).'| tr -d "\017"', 1, <bang>0)

nnoremap <leader>p :Find<cr>
nnoremap <leader>t :Files<cr>

""" ultisnips

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

""" tagbar
nmap <F8> :TagbarToggle<CR>
