Custom Vim Settings
===================

Clone this repository as `.vim` in your home directory.

```shell
rm -rf ~/.vimrc
git clone git@gitlab.com:hephex/mydearvim.git ~/.vim
git submodule init
git submodule update --init
```

Add `export TERM=screen-256color` to `.bashrc`/`.zshrc` and
`set -g default-terminal "screen-256color"` to `.tmux.conf`.

Open `.vimrc` and source the base config as first line

```
source $HOME/.vim/vimrc

" Add more specific settings here
" ...
```

Run `rake make` in order to use *Command-t*.

Plugins
-------

This set-up uses [pathogen](https://github.com/tpope/vim-pathogen) as
plugins manager.

[Airline](https://github.com/vim-airline/vim-airline) is a lean and mean
status/tabline for vim that's light as air.

[BetterWhitespace](https://github.com/ntpeters/vim-better-whitespace) causes
all trailing whitespace characters (spaces and tabs) to be highlighted.

[Command-t](https://github.com/wincent/command-t) provides an extremely fast
"fuzzy" mechanism for opening files and buffers, jumping to tags and help,
running commands, or previous searches and commands with a minimal number of
keystrokes.

[EasyMotion](https://github.com/easymotion/vim-easymotion) provides a much
simpler way to use some motions in vim. It takes the `<number>` out of
`<number>w` or `<number>f{char}` by highlighting all possible choices and
allowing you to press one key to jump directly to the target.

[Fugitive](https://github.com/tpope/vim-fugitive) is a git wrapper for vim.

[GitGutter](https://github.com/airblade/vim-gitgutter) is a Vim plugin which
shows a git diff in the 'gutter' (sign column). It shows whether each line has
been added, modified, and where lines have been removed. You can also stage and
undo individual hunks.

[Go](https://github.com/fatih/vim-go) support for Vim, which comes with
pre-defined sensible settings (like auto gofmt on save), with autocomplete,
snippet support, improved syntax highlighting, go toolchain commands, and more.

[NerdCommenter](https://github.com/scrooloose/nerdcommenter) is a utility
to improve code commenting.

[NerdTree](https://github.com/scrooloose/nerdtree) allows you to explore your
filesystem and to open files and directories. It presents the filesystem to you
in the form of a tree which you manipulate with the keyboard and/or mouse.
It also allows you to perform simple filesystem operations.

[PtyhonSyntax](https://github.com/hdima/python-syntax) is an enhanced version
of the original Python syntax highlighting script.

[Rust](https://github.com/rust-lang/rust.vim) is a Vim plugin that provides
Rust file detection, syntax highlighting, formatting, Syntastic integration,
and more.

[Syntastic](https://github.com/vim-syntastic/syntastic) is a syntax checking
plugin for Vim that runs files through external syntax checkers and displays
any resulting errors to the user.

[TOML](https://github.com/cespare/vim-toml) adds syntax support for TOML.

